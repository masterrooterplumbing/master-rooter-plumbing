Master Rooter Plumbers have been the first choice of homeowners living in Southern Idaho for over 70 years and 3 generations. Having won the trust and support of homeowners, we pride ourselves on our delivery of superior services and exceptional performance.

Address: 2012 4th Ave E, Twin Falls, ID 83301, USA

Phone: 208-734-6900
